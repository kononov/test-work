<?php

namespace LaptopRu\Component\Resource\Model;

/**
 * Interface NameableModelInterface
 * @package LaptopRu\Component\Resource\Model
 */
interface NameableModelInterface extends ModelInterface
{
    /**
     * @return string
     */
    public function getName(): string;
}
