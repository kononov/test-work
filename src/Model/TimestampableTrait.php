<?php

namespace LaptopRu\Component\Resource\Model;

/**
 * Trait TimestampableTrait
 * @package LaptopRu\Component\Resource\Model
 */
trait TimestampableTrait
{
    /**
     * @var null
     */
    public $created_at = null;

    /**
     * @var null
     */
    public $updated_at = null;

    function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    /**
     * @param \DateTimeInterface|null $date
     *
     * @return \DateTimeInterface|null
     */
    function setCreatedAt(\DateTimeInterface $date = null)
    {
        $this->created_at = $date;

        return $this->created_at;
    }

    /**
     * @return \DateTimeInterface|null
     */
    function getUpdatedAt(): ?\DateTimeInterface
    {
        return $this->updated_at;
    }

    /**
     * @param \DateTimeInterface|null $date
     *
     * @return \DateTimeInterface|null
     */
    function setUpdatedAt(\DateTimeInterface $date = null)
    {
        $this->updated_at = $date;

        return $this->updated_at;
    }
}
