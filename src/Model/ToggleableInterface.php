<?php

namespace LaptopRu\Component\Resource\Model;

use phpDocumentor\Reflection\Types\Boolean;

/**
 * Interface ToggleableInterface
 * @package LaptopRu\Component\Resource\Model
 */
interface ToggleableInterface
{
    /**
     * @return bool
     */
    public function isEnabled(): bool;

    /**
     * @param bool $param
     *
     * @return mixed
     */
    public function setEnabled(bool $param): bool;

    /**
     * @return mixed
     */
    public function enable(): bool;

    /**
     * @return mixed
     */
    public function disable(): bool;
}
