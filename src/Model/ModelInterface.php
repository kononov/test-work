<?php

namespace LaptopRu\Component\Resource\Model;

/**
 * Interface ModelInterface
 * @package LaptopRu\Component\Resource\Model
 */
interface ModelInterface
{
    /**
     * @return mixed
     */
    public function getId();
}
