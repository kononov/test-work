<?php

namespace LaptopRu\Component\Resource\Model;

/**
 * Interface TimestampableInterface
 * @package LaptopRu\Component\Resource\Model
 */
interface TimestampableInterface
{
    /**
     * @return \DateTimeInterface|null
     */
    public function getCreatedAt(): ?\DateTimeInterface;

    /**
     * @param \DateTimeInterface|null $dateTime
     *
     * @return mixed
     */
    public function setCreatedAt(\DateTimeInterface $dateTime = null);

    /**
     * @return \DateTimeInterface|null
     */
    public function getUpdatedAt(): ?\DateTimeInterface;

    /**
     * @param \DateTimeInterface|null $dateTime
     *
     * @return mixed
     */
    public function setUpdatedAt(\DateTimeInterface $dateTime = null);
}
