<?php

namespace LaptopRu\Component\Resource\Model;

/**
 * Trait ToggleableTrait
 * @package LaptopRu\Component\Resource\Model
 */
trait ToggleableTrait
{
    /**
     * @var bool
     */
    public bool $enabled = true;

    /**
     * @return bool
     */
    function isEnabled(): bool
    {
        return $this->enabled;
    }

    /**
     * @param bool $param
     *
     * @return bool
     */
    function setEnabled(bool $param)
    {
        $this->enabled = $param;

        return $this->enabled;
    }

    /**
     * @return bool
     */
    function enable()
    {
        $this->enabled = true;

        return $this->enabled;
    }

    /**
     * @return bool
     */
    function disable()
    {
        $this->enabled = false;

        return $this->enabled;
    }
}
