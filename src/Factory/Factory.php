<?php

namespace LaptopRu\Component\Resource\Factory;

/**
 * Class Factory
 * @package LaptopRu\Component\Resource\Factory
 */
class Factory implements FactoryInterface {

    /**
     * @return \stdClass
     */
    public function createNew(): \stdClass
    {
        return new \stdClass;
    }

    // test comment
}
