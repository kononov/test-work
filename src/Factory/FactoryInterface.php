<?php

namespace LaptopRu\Component\Resource\Factory;

/**
 * Interface FactoryInterface
 * @package LaptopRu\Component\Resource\Factory
 */
interface FactoryInterface
{
    /**
     * @return mixed
     */
    public function createNew();

}
